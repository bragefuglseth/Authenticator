# Persian translation for authenticator.
# Copyright (C) 2023 authenticator's COPYRIGHT HOLDER
# This file is distributed under the same license as the authenticator package.
# Danial Behzadi <dani.behzi@ubuntu.com>, 2023-2025.
#
msgid ""
msgstr ""
"Project-Id-Version: authenticator master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Authenticator/issues\n"
"POT-Creation-Date: 2025-02-10 11:13+0000\n"
"PO-Revision-Date: 2025-02-11 02:07+0330\n"
"Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>\n"
"Language-Team: Persian <fa@li.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.5\n"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:3
#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:40 src/application.rs:112 src/main.rs:40
msgid "Authenticator"
msgstr "هویت‌سنج"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:4
msgid "Two-Factor Authentication"
msgstr "هویت‌سنجی دو عامله"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:5
#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:8 src/application.rs:114
msgid "Generate two-factor codes"
msgstr "ایجاد کدهای عامل دوم"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:10
msgid "Gnome;GTK;Verification;2FA;Authentication;OTP;TOTP;"
msgstr "Gnome;GTK;Verification;2FA;Authentication;OTP;TOTP;هویت;عامل;"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:6
#: data/com.belmoussaoui.Authenticator.gschema.xml.in:7
msgid "Default window width"
msgstr "پهنای پیش‌گزیدهٔ پنجره"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:11
#: data/com.belmoussaoui.Authenticator.gschema.xml.in:12
msgid "Default window height"
msgstr "بلندای پیش‌گزیدهٔ پنجره"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:16
msgid "Default window maximized behaviour"
msgstr "رفتار پیش‌گزیدهٔ پنجرهٔ بیشینه شده"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:21
msgid "Auto lock"
msgstr "قفل خودکار"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:22
msgid "Whether to auto lock the application or not"
msgstr "این که برنامه به طور خودکار قفل شود یا نه"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:26
msgid "Auto lock timeout"
msgstr "زمان قفل خودکار"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:27
msgid "Lock the application on idle after X minutes"
msgstr "قفل کردن برنامه در حالت بی‌کار پس از x دقیقه"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:34
msgid "Download Favicons"
msgstr "بارگیری نقشک‌ها"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:35
msgid "Whether the application should attempt to find an icon for the providers."
msgstr "این که برنامه باید برای یافتن نقشکی برای فراهم کننده تلاش کند یا نه."

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:39
msgid "Download Favicons over metered connections"
msgstr "بارگیر نقشک‌ها روی اتّصال‌های اندازه گیری شده"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:40
msgid "Whether the application should download favicons over a metered connection."
msgstr "این که برنامه باید نقشک‌ها را روی اتّصال‌های اندازه گیری شده بار بگیرد یا نه."

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:10
msgid "Simple application for generating Two-Factor Authentication Codes."
msgstr "برنامه‌ای ساده برای ایجاد کدهای هویت‌سنجی دو عامله."

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:11
msgid "Features:"
msgstr "ویژگی‌ها:"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:13
msgid "Time-based/Counter-based/Steam methods support"
msgstr "پشتیبانی از روش‌های استیم، مبتنی بر زمان و مبتنی بر شمارش"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:14
msgid "SHA-1/SHA-256/SHA-512 algorithms support"
msgstr "پشتیبانی از الگوریتم‌های SHA-1/SHA-256/SHA-512"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:15
msgid "QR code scanner using a camera or from a screenshot"
msgstr "پویشگر کد پاس با استفاده از دوربین یا نماگرفت"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:16
msgid "Lock the application with a password"
msgstr "قفل برنامه با گذرواژه"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:17
msgid "Beautiful UI"
msgstr "میانای کاربری زیبا"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:18
msgid "GNOME Shell search provider"
msgstr "فراهم کنندهٔ جست‌وجوی پوستهٔ گنوم"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:19
msgid ""
"Backup/Restore from/into known applications like FreeOTP+, Aegis (encrypted / "
"plain-text), andOTP, Google Authenticator"
msgstr ""
"پشتیبان گرفتن در و بازگردانی از برنامه‌های شناخته شده‌ای چون ‪FreeOTP+‬، اجیس "
"(رمزنگاشته و متن‌خام)، andOTP و هویت‌سنچ گوگل"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:25
msgid "Main Window"
msgstr "پنجرهٔ اصلی"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:29
#: data/resources/ui/account_add.ui:30 data/resources/ui/account_add.ui:47
msgid "Add a New Account"
msgstr "افزودن حسابی جدید"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:33
msgid "Add a New Provider"
msgstr "افزودن فراهم کنندهٔ جدید"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:37
msgid "Account Details"
msgstr "جزییات حساب"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:41
msgid "Backup/Restore formats support"
msgstr "پشتیبانی قالب‌های پشتیبان گیری و بازگردانی"

#: data/resources/ui/account_add.ui:6 src/widgets/backup/dialog.rs:274
msgid "_Camera"
msgstr "_دوربین"

#: data/resources/ui/account_add.ui:10 src/widgets/backup/dialog.rs:278
msgid "_Screenshot"
msgstr "_نماگرفت"

#: data/resources/ui/account_add.ui:14 src/widgets/backup/dialog.rs:283
msgid "_QR Code Image"
msgstr "_تصویر کد پاس"

#: data/resources/ui/account_add.ui:56
msgid "_Cancel"
msgstr "_لغو"

#: data/resources/ui/account_add.ui:63
msgid "_Add"
msgstr "_افزودن"

#: data/resources/ui/account_add.ui:75 src/widgets/backup/dialog.rs:269
msgid "Scan QR Code"
msgstr "پویش کد پاس"

#: data/resources/ui/account_add.ui:113 data/resources/ui/account_details_page.ui:96
#: data/resources/ui/provider_page.ui:22 data/resources/ui/providers_dialog.ui:108
msgid "Provider"
msgstr "فراهم کننده"

#: data/resources/ui/account_add.ui:114
msgid "Token issuer"
msgstr "صادرکنندهٔ ژتون"

#: data/resources/ui/account_add.ui:128 data/resources/ui/account_details_page.ui:91
msgid "Account"
msgstr "حساب"

#: data/resources/ui/account_add.ui:134
msgid "Token"
msgstr "ژتون"

#: data/resources/ui/account_add.ui:140 data/resources/ui/account_details_page.ui:135
#: data/resources/ui/provider_page.ui:158
msgid "Counter"
msgstr "شمارنده"

#: data/resources/ui/account_add.ui:158 data/resources/ui/account_details_page.ui:119
#: data/resources/ui/provider_page.ui:133
msgid "Algorithm"
msgstr "الگوریتم"

#: data/resources/ui/account_add.ui:166 data/resources/ui/account_details_page.ui:127
#: data/resources/ui/provider_page.ui:118
msgid "Computing Method"
msgstr "روش محاسبه"

#: data/resources/ui/account_add.ui:174 data/resources/ui/account_details_page.ui:144
#: data/resources/ui/provider_page.ui:149
msgid "Period"
msgstr "دوره"

#: data/resources/ui/account_add.ui:175 data/resources/ui/account_details_page.ui:145
#: data/resources/ui/provider_page.ui:150
msgid "Duration in seconds until the next password update"
msgstr "مدّت به ثانیه تا به‌روز رسانی بعدی گذرواژه"

#: data/resources/ui/account_add.ui:183 data/resources/ui/account_details_page.ui:153
#: data/resources/ui/provider_page.ui:167
msgid "Digits"
msgstr "ارقام"

#: data/resources/ui/account_add.ui:184 data/resources/ui/account_details_page.ui:154
#: data/resources/ui/provider_page.ui:168
msgid "Length of the generated code"
msgstr "طول کد ایجاد شده"

#: data/resources/ui/account_add.ui:192 data/resources/ui/account_details_page.ui:162
#: data/resources/ui/provider_page.ui:112
msgid "Website"
msgstr "پایگاه وب"

#: data/resources/ui/account_add.ui:197
msgid "How to Set Up"
msgstr "چگونگی برپایی"

#: data/resources/ui/account_add.ui:217
#: data/resources/ui/preferences_camera_page.ui:4
msgid "Camera"
msgstr "دوربین"

#: data/resources/ui/account_add.ui:229
msgid "Create Provider"
msgstr "ایجاد فراهم کننده"

#: data/resources/ui/account_details_page.ui:56
#: data/resources/ui/preferences_password_page.ui:17
#: data/resources/ui/provider_page.ui:29
msgid "_Save"
msgstr "_ذخیره"

#: data/resources/ui/account_details_page.ui:67
#: data/resources/ui/provider_page.ui:185
msgid "_Delete"
msgstr "_حذف"

#: data/resources/ui/account_details_page.ui:167
msgid "Help"
msgstr "راهنما"

#: data/resources/ui/account_row.ui:23
msgid "Increment the counter"
msgstr "افزایش شمارنده"

#: data/resources/ui/account_row.ui:34
msgid "Copy PIN to clipboard"
msgstr "رونوشت از پین به تخته‌گیره"

#: data/resources/ui/account_row.ui:43
msgid "Account details"
msgstr "جزییات حساب"

#: data/resources/ui/backup_dialog.ui:4 data/resources/ui/backup_dialog.ui:8
msgid "Backup & Restore"
msgstr "پشتیبان گیری و بازگردانی"

#: data/resources/ui/backup_dialog.ui:11 src/widgets/backup/dialog.rs:506
msgid "Backup"
msgstr "پشتیبان گیری"

#: data/resources/ui/backup_dialog.ui:16 src/widgets/backup/dialog.rs:514
msgid "Restore"
msgstr "بازآوری"

#: data/resources/ui/camera.ui:35
msgid "No Camera Found"
msgstr "هیچ دوربینی پیدا نشد"

#: data/resources/ui/camera.ui:38
msgid "_From a Screenshot"
msgstr "از _نماگرفت"

#: data/resources/ui/keyring_error_dialog.ui:8
msgid "Secret Service Error"
msgstr "خطای خدمت رمزی"

#: data/resources/ui/keyring_error_dialog.ui:35
msgid ""
"Authenticator relies on a Secret Service Provider to manage your sensitive session "
"information and an error occurred while we were trying to store or get your "
"session."
msgstr ""
"هویت‌سنج برای مدیریت اطّلاعات نشست حسّاستان متّکی به یک فراهم‌کنندهٔ خدمت رمزیست و خطایی "
"هنگام تلاش برای ذخیره یا گرفتن نشستتان رخ داد."

#: data/resources/ui/keyring_error_dialog.ui:47
msgid "Here are a few things that might help you fix issues with the Secret Service:"
msgstr "این‌جا چیزهایی برای کمک به رفع مشکل با خدمت رمزی هست:"

#: data/resources/ui/keyring_error_dialog.ui:64
msgid "Make sure you have a Secret Service Provider installed, like gnome-keyring."
msgstr "مطمئن شوید که فراهم‌کنندهٔ خدمت رمزی‌ای چون gnome-keyring را نصب دارید."

#: data/resources/ui/keyring_error_dialog.ui:83
msgid "Check that you have a default keyring and that it is unlocked."
msgstr "بررسی کنید که دسته‌کلید پیش‌گزیده را دارید و قفلش گشوده است."

#: data/resources/ui/keyring_error_dialog.ui:95
msgid ""
"Check the application logs and your distribution’s documentation for more details."
msgstr "برای جزییات بیش‌تر گزارش‌های برنامه و مستندات توزیعتان را بررسی کنید."

#: data/resources/ui/preferences.ui:14
msgid "General"
msgstr "عمومی"

#: data/resources/ui/preferences.ui:17
msgid "Privacy"
msgstr "محرمانگی"

#: data/resources/ui/preferences.ui:20
msgid "_Passphrase"
msgstr "_عبارت عبور"

#: data/resources/ui/preferences.ui:22
msgid "Set up a passphrase to lock the application with"
msgstr "برپایی یک عبارت عبور برای قفل برنامه"

#: data/resources/ui/preferences.ui:35
msgid "_Auto Lock the Application"
msgstr "_قفل خودکار برنامه"

#: data/resources/ui/preferences.ui:37
msgid "Whether to automatically lock the application"
msgstr "این که برنامه به طور خودکار قفل شود یا نه"

#: data/resources/ui/preferences.ui:43
msgid "Auto Lock _Timeout"
msgstr "_زمان قفل خودکار"

#: data/resources/ui/preferences.ui:44
msgid "The time in minutes"
msgstr "زمان به دقیقه"

#: data/resources/ui/preferences.ui:57
msgid "Network"
msgstr "شبکه"

#: data/resources/ui/preferences.ui:60
msgid "_Download Favicons"
msgstr "_بارگیری نقشک‌ها"

#: data/resources/ui/preferences.ui:62
msgid "Automatically attempt fetching a website icon"
msgstr "تلاش خودکار برای گرفتن نقشک پایگاه وب"

#: data/resources/ui/preferences.ui:67
msgid "_Metered Connection"
msgstr "اتّصال _اندازه‌گیری شده"

#: data/resources/ui/preferences.ui:69
msgid "Fetch a website icon on a metered connection"
msgstr "گرفتن نقشک پیاگاه وب روی اتّصال اندازه‌گیری شده"

#: data/resources/ui/preferences_password_page.ui:4
msgid "Create Password"
msgstr "ایجاد گذرواژه"

#: data/resources/ui/preferences_password_page.ui:28
msgid "Set up a Passphrase"
msgstr "برپایی یک عبارت عبور"

#: data/resources/ui/preferences_password_page.ui:29
msgid "Authenticator will start locked after a passphrase is set."
msgstr "پس از برپایی عبارت عبور، هویت‌سنج قفل‌شده آغاز خواهد شد."

#: data/resources/ui/preferences_password_page.ui:53
msgid "Current Passphrase"
msgstr "عبارت عبور کنونی"

#: data/resources/ui/preferences_password_page.ui:59
msgid "New Passphrase"
msgstr "عبارت‌عبور جدید"

#: data/resources/ui/preferences_password_page.ui:65
msgid "Repeat Passphrase"
msgstr "تکرار عبارت عبور"

#: data/resources/ui/preferences_password_page.ui:79
#: data/resources/ui/provider_page.ui:73
msgid "_Reset"
msgstr "_بازنشانی"

#: data/resources/ui/provider_page.ui:83
msgid "Select a _File"
msgstr "گزینش _پرونده"

#: data/resources/ui/provider_page.ui:106
msgid "Name"
msgstr "نام"

#: data/resources/ui/provider_page.ui:159
msgid "The by default value for counter-based computing method"
msgstr "مقدار پیش‌گزیده برای روش محاسبهٔ مبتنی بر شمارش"

#: data/resources/ui/provider_page.ui:177
msgid "Help URL"
msgstr "نشانی راهنما"

#: data/resources/ui/providers_dialog.ui:8 data/resources/ui/providers_dialog.ui:19
msgid "Providers"
msgstr "فراهم کنندگان"

#: data/resources/ui/providers_dialog.ui:29 src/widgets/providers/page.rs:208
msgid "New Provider"
msgstr "فراهم کنندهٔ جدید"

#: data/resources/ui/providers_dialog.ui:36 data/resources/ui/providers_dialog.ui:52
#: data/resources/ui/window.ui:198 data/resources/ui/window.ui:218
msgid "Search"
msgstr "جست‌وجو"

#: data/resources/ui/providers_dialog.ui:47 data/resources/ui/window.ui:213
msgid "Search…"
msgstr "جست‌وجو…"

#: data/resources/ui/providers_dialog.ui:93 data/resources/ui/providers_list.ui:38
msgid "No Results"
msgstr "بدون نتیجه"

#: data/resources/ui/providers_dialog.ui:94
msgid "No providers matching the query were found."
msgstr "هیچ فراهم کنندهٔ مطابقی با پرسش‌وجو پیدا نشد."

#: data/resources/ui/providers_dialog.ui:139
msgid "No Provider Selected"
msgstr "هیچ فراهم کننده‌ای گزیده نشده"

#: data/resources/ui/providers_dialog.ui:140
msgid "Select a provider or create a new one"
msgstr "گزینش یا ایجاد فراهم کننده"

#: data/resources/ui/providers_list.ui:39
msgid "No accounts or providers matching the query were found."
msgstr "هیچ حساب یا فراهم کنندهٔ مطابقی با پرسش‌وجو پیدا نشد."

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "عمومی"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "میان‌برهای صفحه‌کلید"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Lock"
msgstr "قفل کردن"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Preferences"
msgstr "ترجیحات"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "خروج"

#: data/resources/ui/shortcuts.ui:40
msgctxt "shortcut window"
msgid "Accounts"
msgstr "حساب"

#: data/resources/ui/shortcuts.ui:43
msgctxt "shortcut window"
msgid "New Account"
msgstr "حساب جدید"

#: data/resources/ui/shortcuts.ui:49
msgctxt "shortcut window"
msgid "Search"
msgstr "جست‌وجو"

#: data/resources/ui/shortcuts.ui:55
msgctxt "shortcut window"
msgid "Show Providers List"
msgstr "نمایش سیاههٔ فراهم کنندگان"

#: data/resources/ui/window.ui:6
msgid "_Lock the Application"
msgstr "_قفل برنامه"

#: data/resources/ui/window.ui:12
msgid "P_roviders"
msgstr "_فراهم کنندگان"

#: data/resources/ui/window.ui:16
msgid "_Preferences"
msgstr "_ترجیحات"

#: data/resources/ui/window.ui:20
msgid "_Backup & Restore"
msgstr "-پشتیبان گیری و بازگردانی"

#: data/resources/ui/window.ui:26
msgid "_Keyboard Shortcuts"
msgstr "_میان‌برهای صفحه‌کلید"

#: data/resources/ui/window.ui:30
msgid "_About Authenticator"
msgstr "_دربارهٔ هویت‌سنج"

#: data/resources/ui/window.ui:70
msgid "Authenticator is Locked"
msgstr "هویت‌سنج قفل شده"

#: data/resources/ui/window.ui:96
msgid "_Unlock"
msgstr "قفل‌_گشایی"

#: data/resources/ui/window.ui:126
msgid "Accounts"
msgstr "حساب"

#: data/resources/ui/window.ui:141 data/resources/ui/window.ui:179
msgid "New Account"
msgstr "حساب جدید"

#: data/resources/ui/window.ui:149 data/resources/ui/window.ui:192
msgid "Main Menu"
msgstr "فهرست اصلی"

#: data/resources/ui/window.ui:157
msgid "No Accounts"
msgstr "بدون حساب"

#: data/resources/ui/window.ui:158
msgid "Add an account or scan a QR code first."
msgstr "نخست حسابی افزوده یا کد پاسی را بپویید."

#: src/application.rs:83
msgid "Accounts restored successfully"
msgstr "حساب‌ها با موفّقیت بازگردانده شدند"

#: src/application.rs:123
msgid "translator-credits"
msgstr ""
"آرش موسوی <mousavi.arash@gmail.com>\n"
"دانیال بهزادی <dani.behzi@ubuntu.com>\n"
"روزبه پورنادر <roozbeh@farsiweb.info>‏\n"
"میلاد زکریا <meelad@farsiweb.info>‏\n"
"الناز سربر <elnaz@farsiweb.info>\n"
"مهیار مقیمی <mahyar.moqimi@gmail.com>"

#: src/application.rs:434 src/widgets/accounts/row.rs:45
msgid "One-Time password copied"
msgstr "گذرواژهٔ یک‌بار مصرف رونوشت شد"

#: src/application.rs:435
msgid "Password was copied successfully"
msgstr "گذرواژه با موفّقیت رونوشت شد"

#. Translators: This is for making a backup for the aegis Android app.
#. Translators: This is for restoring a backup from the aegis Android app.
#: src/backup/aegis.rs:399 src/backup/aegis.rs:439
msgid "Aegis"
msgstr "اجیس"

#: src/backup/aegis.rs:403
msgid "Into a JSON file containing plain-text or encrypted fields"
msgstr "در یک پروندهٔ‌ JSON شامل متن خام یا زمینه‌های رمز شده"

#: src/backup/aegis.rs:443
msgid "From a JSON file containing plain-text or encrypted fields"
msgstr "از یک پروندهٔ‌ JSON شامل متن خام یا زمینه‌های رمز شده"

#. Translators: This is for making a backup for the andOTP Android app.
#: src/backup/andotp.rs:79
msgid "a_ndOTP"
msgstr "a_ndOTP"

#: src/backup/andotp.rs:83
msgid "Into a plain-text JSON file"
msgstr "به یک پروندهٔ خام JSON"

#. Translators: This is for restoring a backup from the andOTP Android app.
#: src/backup/andotp.rs:127
msgid "an_dOTP"
msgstr "an_dOTP"

#: src/backup/andotp.rs:131 src/backup/bitwarden.rs:124 src/backup/legacy.rs:45
msgid "From a plain-text JSON file"
msgstr "از یک پروندهٔ خام JSON"

#: src/backup/bitwarden.rs:47
msgid "Unknown account"
msgstr "حساب ناشناخته"

#: src/backup/bitwarden.rs:55
msgid "Unknown issuer"
msgstr "صادرکنندهٔ ناشناخته"

#. Translators: This is for restoring a backup from Bitwarden.
#: src/backup/bitwarden.rs:120
msgid "_Bitwarden"
msgstr "_بیت‌واردن"

#: src/backup/freeotp.rs:18
msgid "_Authenticator"
msgstr "_هویت‌سنج"

#: src/backup/freeotp.rs:22
msgid "Into a plain-text file, compatible with FreeOTP+"
msgstr "به یک پروندهٔ خام متنی سازگار با ‪FreeOTP+"

#: src/backup/freeotp.rs:51
msgid "A_uthenticator"
msgstr "هویت‌_سنج"

#: src/backup/freeotp.rs:55
msgid "From a plain-text file, compatible with FreeOTP+"
msgstr "از یک پروندهٔ خام متنی سازگار با ‪FreeOTP+"

#: src/backup/freeotp_json.rs:87
msgid "FreeOTP+"
msgstr "FreeOTP+"

#: src/backup/freeotp_json.rs:91
msgid "From a plain-text JSON file, compatible with FreeOTP+"
msgstr "از یک پروندهٔ خام JSON سازگار با ‪FreeOTP+"

#: src/backup/google.rs:19
msgid "Google Authenticator"
msgstr "هویت‌سنچ گوگل"

#: src/backup/google.rs:23
msgid "From a QR code generated by Google Authenticator"
msgstr "از یک کد پاس ایجاد شده به دست هویت‌سنج گوگل"

#. Translators: this is for restoring a backup from the old Authenticator
#. release
#: src/backup/legacy.rs:41
msgid "Au_thenticator (Legacy)"
msgstr "هوی_ت‌سنچ (قدیمی)"

#: src/backup/raivootp.rs:102
msgid "Raivo OTP"
msgstr "راویو OTP"

#: src/backup/raivootp.rs:106
msgid "From a ZIP export generated by Raivo OTP"
msgstr "از برون‌ریزی فشردهٔ راویو OTP"

#: src/models/algorithm.rs:60
msgid "Counter-based"
msgstr "مبتنی بر شمارش"

#: src/models/algorithm.rs:61
msgid "Time-based"
msgstr "مبتنی بر زمان"

#. Translators: Steam refers to the gaming application by Valve.
#: src/models/algorithm.rs:63
msgid "Steam"
msgstr "استیم"

#: src/models/algorithm.rs:125
msgid "SHA-1"
msgstr "SHA-1"

#: src/models/algorithm.rs:126
msgid "SHA-256"
msgstr "SHA-256"

#: src/models/algorithm.rs:127
msgid "SHA-512"
msgstr "SHA-512"

#: src/widgets/accounts/add.rs:270 src/widgets/backup/dialog.rs:443
#: src/widgets/providers/page.rs:291
msgid "Image"
msgstr "تصویر"

#: src/widgets/accounts/add.rs:278 src/widgets/backup/dialog.rs:451
msgid "Select QR Code"
msgstr "گزینش کد پاس"

#: src/widgets/accounts/add.rs:296
msgid "Invalid Token"
msgstr "ژتون نامعتبر"

#: src/widgets/accounts/details.rs:123
msgid "Are you sure you want to delete the account?"
msgstr "مطمئنید که می‌خواهید این حساب را حذف کنید؟"

#: src/widgets/accounts/details.rs:124
msgid "This action is irreversible"
msgstr "این کنش بازگشت‌ناپذیر است"

#: src/widgets/accounts/details.rs:126
msgid "No"
msgstr "نه"

#: src/widgets/accounts/details.rs:126
msgid "Yes"
msgstr "بله"

#: src/widgets/backup/dialog.rs:163 src/widgets/backup/dialog.rs:245
msgid "Key / Passphrase"
msgstr "کلید یا عبارت عبور"

#: src/widgets/backup/dialog.rs:174 src/widgets/backup/dialog.rs:257
msgid "Select File"
msgstr "انتخاب پرونده"

#: src/widgets/backup/dialog.rs:205
msgid "Failed to create a backup"
msgstr "شکست در ایجاد پشتیبان"

#: src/widgets/backup/dialog.rs:327
msgid "Failed to restore from camera"
msgstr "شکست در بازگردانی از دوربین"

#: src/widgets/backup/dialog.rs:352
msgid "Failed to restore from a screenshot"
msgstr "شکست در بازگردانی از نماگرفت"

#: src/widgets/backup/dialog.rs:372
msgid "Failed to restore from an image"
msgstr "شکست در بازگردانی از تصویر"

#: src/widgets/backup/dialog.rs:396
msgid "Failed to restore from a file"
msgstr "شکست در بازگردانی از پرونده"

#: src/widgets/preferences/password_page.rs:195
#: src/widgets/preferences/password_page.rs:237
msgid "Wrong Passphrase"
msgstr "عبارت عبور اشتباه"

#: src/widgets/providers/dialog.rs:261
msgid "Provider created successfully"
msgstr "فراهم کننده با موفّقیت ساخته شد"

#: src/widgets/providers/dialog.rs:271
msgid "Provider updated successfully"
msgstr "فراهم کننده با موفّقیت به‌روز شد"

#: src/widgets/providers/dialog.rs:287
msgid "Provider removed successfully"
msgstr "فراهم کننده با موفّقیت برداشته شد"

#: src/widgets/providers/page.rs:185
msgid "Editing Provider: {}"
msgstr "ویرایش کردن فراهم کننده: {}"

#: src/widgets/providers/page.rs:324
msgid "The provider has accounts assigned to it, please remove them first"
msgstr "فراهم کننده حساب‌های مرتبطی دارد. لطفاً نخست آن‌ها را بردارید"

#: src/widgets/window.rs:115
msgid "Wrong Password"
msgstr "گذرواژهٔ اشتباه"

#~ msgid "Bilal Elmoussaoui"
#~ msgstr "بلال الموسایی"
