# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Authenticator package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Rodrigo Lledó <rodhos92@gmail.com>, 2019.
# Óscar Fernández Díaz <oscfdezdz@tuta.io>, 2020.
# Daniel Mustieles <daniel.mustieles@gmail.com>, 2022-2023.
# Julián Villodre <jvillodrede@gmail.com>, 2024-2025.
#
msgid ""
msgstr ""
"Project-Id-Version: Authenticator\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Authenticator/issues\n"
"POT-Creation-Date: 2025-02-10 11:13+0000\n"
"PO-Revision-Date: 2025-02-15 12:11+0100\n"
"Last-Translator: Julián Villodre <jvillodrede@gmail.com>\n"
"Language-Team: es_ES\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 47.1\n"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:3
#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:40 src/application.rs:112 src/main.rs:40
msgid "Authenticator"
msgstr "Authenticador"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:4
msgid "Two-Factor Authentication"
msgstr "Autenticación de doble factor"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:5
#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:8
#: src/application.rs:114
msgid "Generate two-factor codes"
msgstr "Generar códigos de autenticación de doble factor"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:10
msgid "Gnome;GTK;Verification;2FA;Authentication;OTP;TOTP;"
msgstr "Gnome;GTK;Verificación;2FA;Autenticación;Doble;Factor;OTP;TOPT;"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:6
#: data/com.belmoussaoui.Authenticator.gschema.xml.in:7
msgid "Default window width"
msgstr "Anchura predeterminada de la ventana"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:11
#: data/com.belmoussaoui.Authenticator.gschema.xml.in:12
msgid "Default window height"
msgstr "Altura predeterminada de la ventana"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:16
msgid "Default window maximized behaviour"
msgstr "Comportamiento predeterminado al maximizar la ventana"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:21
msgid "Auto lock"
msgstr "Bloqueo automático"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:22
msgid "Whether to auto lock the application or not"
msgstr "Indica si la aplicación se bloquea automáticamente o no"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:26
msgid "Auto lock timeout"
msgstr "Tiempo de espera del bloqueo automático"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:27
msgid "Lock the application on idle after X minutes"
msgstr "Bloquear la aplicación tras X minutos de inactividad"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:34
msgid "Download Favicons"
msgstr "Descargar favicons"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:35
msgid ""
"Whether the application should attempt to find an icon for the providers."
msgstr ""
"Indica si la aplicación debe intentar buscar un icono para los proveedores."

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:39
msgid "Download Favicons over metered connections"
msgstr "Descargar favicons en conexiones medidas"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:40
msgid ""
"Whether the application should download favicons over a metered connection."
msgstr ""
"Indica si la aplicación debe intentar descargar favicons en conexiones "
"medidas."

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:10
msgid "Simple application for generating Two-Factor Authentication Codes."
msgstr ""
"Aplicación sencilla para generar códigos de autenticación de doble factor."

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:11
msgid "Features:"
msgstr "Características:"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:13
msgid "Time-based/Counter-based/Steam methods support"
msgstr "Soporte para métodos Basado en tiempo/Basado en contador/Steam"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:14
msgid "SHA-1/SHA-256/SHA-512 algorithms support"
msgstr "Soporte de algoritmos SHA-1/SHA-256/SHA-512"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:15
msgid "QR code scanner using a camera or from a screenshot"
msgstr ""
"Analizador de códigos QR usando la cámara o a partir de una captura de "
"pantalla"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:16
msgid "Lock the application with a password"
msgstr "Bloquear la aplicación con una contraseña"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:17
msgid "Beautiful UI"
msgstr "Excelente interfaz de usuario"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:18
msgid "GNOME Shell search provider"
msgstr "Proveedor de búsqueda de GNOME Shell"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:19
msgid ""
"Backup/Restore from/into known applications like FreeOTP+, Aegis "
"(encrypted / plain-text), andOTP, Google Authenticator"
msgstr ""
"Respaldar/Restaurar desde/en aplicaciones conocidas como FreeOTP+, Aegis "
"(cifrado / texto plano), andOTP, Google Authenticator"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:25
msgid "Main Window"
msgstr "Ventana principal"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:29
#: data/resources/ui/account_add.ui:30 data/resources/ui/account_add.ui:47
msgid "Add a New Account"
msgstr "Añadir una cuenta nueva"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:33
msgid "Add a New Provider"
msgstr "Añadir un proveedor nuevo"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:37
msgid "Account Details"
msgstr "Detalles de la cuenta"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:41
msgid "Backup/Restore formats support"
msgstr "Soporte de formatos de Respaldar/Restaurar"

#: data/resources/ui/account_add.ui:6 src/widgets/backup/dialog.rs:274
msgid "_Camera"
msgstr "_Cámara"

#: data/resources/ui/account_add.ui:10 src/widgets/backup/dialog.rs:278
msgid "_Screenshot"
msgstr "_Captura de pantalla"

#: data/resources/ui/account_add.ui:14 src/widgets/backup/dialog.rs:283
msgid "_QR Code Image"
msgstr "Imagen de código _QR"

#: data/resources/ui/account_add.ui:56
msgid "_Cancel"
msgstr "_Cancelar"

#: data/resources/ui/account_add.ui:63
msgid "_Add"
msgstr "_Añadir"

#: data/resources/ui/account_add.ui:75 src/widgets/backup/dialog.rs:269
msgid "Scan QR Code"
msgstr "Escanear código QR"

#: data/resources/ui/account_add.ui:113
#: data/resources/ui/account_details_page.ui:96
#: data/resources/ui/provider_page.ui:22
#: data/resources/ui/providers_dialog.ui:108
msgid "Provider"
msgstr "Proveedor"

#: data/resources/ui/account_add.ui:114
msgid "Token issuer"
msgstr "Emisor del testigo"

#: data/resources/ui/account_add.ui:128
#: data/resources/ui/account_details_page.ui:91
msgid "Account"
msgstr "Cuenta"

#: data/resources/ui/account_add.ui:134
msgid "Token"
msgstr "Testigo"

#: data/resources/ui/account_add.ui:140
#: data/resources/ui/account_details_page.ui:135
#: data/resources/ui/provider_page.ui:158
msgid "Counter"
msgstr "Contador"

#: data/resources/ui/account_add.ui:158
#: data/resources/ui/account_details_page.ui:119
#: data/resources/ui/provider_page.ui:133
msgid "Algorithm"
msgstr "Algoritmo"

#: data/resources/ui/account_add.ui:166
#: data/resources/ui/account_details_page.ui:127
#: data/resources/ui/provider_page.ui:118
msgid "Computing Method"
msgstr "Método de computación"

#: data/resources/ui/account_add.ui:174
#: data/resources/ui/account_details_page.ui:144
#: data/resources/ui/provider_page.ui:149
msgid "Period"
msgstr "Periodo"

#: data/resources/ui/account_add.ui:175
#: data/resources/ui/account_details_page.ui:145
#: data/resources/ui/provider_page.ui:150
msgid "Duration in seconds until the next password update"
msgstr "Duración en segundos hasta la siguiente actualización de la contraseña"

#: data/resources/ui/account_add.ui:183
#: data/resources/ui/account_details_page.ui:153
#: data/resources/ui/provider_page.ui:167
msgid "Digits"
msgstr "Dígitos"

#: data/resources/ui/account_add.ui:184
#: data/resources/ui/account_details_page.ui:154
#: data/resources/ui/provider_page.ui:168
msgid "Length of the generated code"
msgstr "Longitud del código generado"

#: data/resources/ui/account_add.ui:192
#: data/resources/ui/account_details_page.ui:162
#: data/resources/ui/provider_page.ui:112
msgid "Website"
msgstr "Página web"

#: data/resources/ui/account_add.ui:197
msgid "How to Set Up"
msgstr "Cómo configurar"

#: data/resources/ui/account_add.ui:217
#: data/resources/ui/preferences_camera_page.ui:4
msgid "Camera"
msgstr "Cámara"

#: data/resources/ui/account_add.ui:229
msgid "Create Provider"
msgstr "Crear proveedor"

#: data/resources/ui/account_details_page.ui:56
#: data/resources/ui/preferences_password_page.ui:17
#: data/resources/ui/provider_page.ui:29
msgid "_Save"
msgstr "_Guardar"

#: data/resources/ui/account_details_page.ui:67
#: data/resources/ui/provider_page.ui:185
msgid "_Delete"
msgstr "E_liminar"

#: data/resources/ui/account_details_page.ui:167
msgid "Help"
msgstr "Ayuda"

#: data/resources/ui/account_row.ui:23
msgid "Increment the counter"
msgstr "Incrementar el contador"

#: data/resources/ui/account_row.ui:34
msgid "Copy PIN to clipboard"
msgstr "Copiar PIN al portapapeles"

#: data/resources/ui/account_row.ui:43
msgid "Account details"
msgstr "Detalles de la cuenta"

#: data/resources/ui/backup_dialog.ui:4 data/resources/ui/backup_dialog.ui:8
msgid "Backup & Restore"
msgstr "Respaldar y restaurar"

#: data/resources/ui/backup_dialog.ui:11 src/widgets/backup/dialog.rs:506
msgid "Backup"
msgstr "Respaldar"

#: data/resources/ui/backup_dialog.ui:16 src/widgets/backup/dialog.rs:514
msgid "Restore"
msgstr "Restaurar"

#: data/resources/ui/camera.ui:35
msgid "No Camera Found"
msgstr "No se ha encontrado ninguna cámara"

#: data/resources/ui/camera.ui:38
msgid "_From a Screenshot"
msgstr "_Desde una captura de pantalla"

#: data/resources/ui/keyring_error_dialog.ui:8
msgid "Secret Service Error"
msgstr "Error en el Servicio de Secretos"

#: data/resources/ui/keyring_error_dialog.ui:35
msgid ""
"Authenticator relies on a Secret Service Provider to manage your sensitive "
"session information and an error occurred while we were trying to store or "
"get your session."
msgstr ""
"Authenticator depende de un Proveedor de Secretos para administrar la "
"información confidencial de su sesión. Ocurrió un error al intentar "
"almacenar u obtener su sesión."

#: data/resources/ui/keyring_error_dialog.ui:47
msgid ""
"Here are a few things that might help you fix issues with the Secret Service:"
msgstr ""
"Aquí encontrará información que podría ayudarle a solucionar problemas con "
"el Servicio de Secretos:"

#: data/resources/ui/keyring_error_dialog.ui:64
msgid ""
"Make sure you have a Secret Service Provider installed, like gnome-keyring."
msgstr ""
"Asegúrese de disponer de un Proveedor de Secretos instalado, como gnome-"
"keyring."

#: data/resources/ui/keyring_error_dialog.ui:83
msgid "Check that you have a default keyring and that it is unlocked."
msgstr "Compruebe que tiene un llavero predeterminado y que esté desbloqueado."

#: data/resources/ui/keyring_error_dialog.ui:95
msgid ""
"Check the application logs and your distribution’s documentation for more "
"details."
msgstr ""
"Para más información, compruebe los registros de la aplicación y la "
"documentación de su distribución."

#: data/resources/ui/preferences.ui:14
msgid "General"
msgstr "General"

#: data/resources/ui/preferences.ui:17
msgid "Privacy"
msgstr "Privacidad"

#: data/resources/ui/preferences.ui:20
msgid "_Passphrase"
msgstr "_Frase de paso"

#: data/resources/ui/preferences.ui:22
msgid "Set up a passphrase to lock the application with"
msgstr "Establecer una frase de paso con la que bloquear la aplicación"

#: data/resources/ui/preferences.ui:35
msgid "_Auto Lock the Application"
msgstr "Bloquear _automáticamente de la aplicación"

#: data/resources/ui/preferences.ui:37
msgid "Whether to automatically lock the application"
msgstr "Indica si bloquear automáticamente la aplicación"

#: data/resources/ui/preferences.ui:43
msgid "Auto Lock _Timeout"
msgstr "_Tiempo de espera del bloqueo automático"

#: data/resources/ui/preferences.ui:44
msgid "The time in minutes"
msgstr "El tiempo en minutos"

#: data/resources/ui/preferences.ui:57
msgid "Network"
msgstr "Red"

#: data/resources/ui/preferences.ui:60
msgid "_Download Favicons"
msgstr "_Descargar favicons"

#: data/resources/ui/preferences.ui:62
msgid "Automatically attempt fetching a website icon"
msgstr "Obtener automáticamente una icono de un sitio web"

#: data/resources/ui/preferences.ui:67
msgid "_Metered Connection"
msgstr "Conexión _medida"

#: data/resources/ui/preferences.ui:69
msgid "Fetch a website icon on a metered connection"
msgstr "Obtener el icono de un sitio web con una conexión medida"

#: data/resources/ui/preferences_password_page.ui:4
msgid "Create Password"
msgstr "Crear contraseña"

#: data/resources/ui/preferences_password_page.ui:28
msgid "Set up a Passphrase"
msgstr "Establecer una frase de paso"

#: data/resources/ui/preferences_password_page.ui:29
msgid "Authenticator will start locked after a passphrase is set."
msgstr ""
"Autenticador se iniciará bloqueado después de establecer la frase de paso."

#: data/resources/ui/preferences_password_page.ui:53
msgid "Current Passphrase"
msgstr "Frase de paso actual"

#: data/resources/ui/preferences_password_page.ui:59
msgid "New Passphrase"
msgstr "Frase de paso nueva"

#: data/resources/ui/preferences_password_page.ui:65
msgid "Repeat Passphrase"
msgstr "Repita la frase de paso"

#: data/resources/ui/preferences_password_page.ui:79
#: data/resources/ui/provider_page.ui:73
msgid "_Reset"
msgstr "_Restablecer"

#: data/resources/ui/provider_page.ui:83
msgid "Select a _File"
msgstr "Seleccionar un _archivo"

#: data/resources/ui/provider_page.ui:106
msgid "Name"
msgstr "Nombre"

#: data/resources/ui/provider_page.ui:159
msgid "The by default value for counter-based computing method"
msgstr ""
"El valor predeterminadoc del método de computación basado en el contador"

#: data/resources/ui/provider_page.ui:177
msgid "Help URL"
msgstr "URL de ayuda"

#: data/resources/ui/providers_dialog.ui:8
#: data/resources/ui/providers_dialog.ui:19
msgid "Providers"
msgstr "Proveedores"

#: data/resources/ui/providers_dialog.ui:29 src/widgets/providers/page.rs:208
msgid "New Provider"
msgstr "Nuevo proveedor"

#: data/resources/ui/providers_dialog.ui:36
#: data/resources/ui/providers_dialog.ui:52 data/resources/ui/window.ui:198
#: data/resources/ui/window.ui:218
msgid "Search"
msgstr "Buscar"

#: data/resources/ui/providers_dialog.ui:47 data/resources/ui/window.ui:213
msgid "Search…"
msgstr "Buscar..."

#: data/resources/ui/providers_dialog.ui:93
#: data/resources/ui/providers_list.ui:38
msgid "No Results"
msgstr "No se han encontrado resultados"

#: data/resources/ui/providers_dialog.ui:94
msgid "No providers matching the query were found."
msgstr "No se han encontrado proveedores que coincidan con la consulta."

#: data/resources/ui/providers_dialog.ui:139
msgid "No Provider Selected"
msgstr "Ningún proveedor seleccionado"

#: data/resources/ui/providers_dialog.ui:140
msgid "Select a provider or create a new one"
msgstr "Seleccione un proveedor o cree uno nuevo"

#: data/resources/ui/providers_list.ui:39
msgid "No accounts or providers matching the query were found."
msgstr ""
"No se han encontrado cuentas o proveedores que coincidan con la consulta."

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "General"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "Atajo del teclado"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Lock"
msgstr "Bloquear"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Preferences"
msgstr "Preferencias"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Salir"

#: data/resources/ui/shortcuts.ui:40
msgctxt "shortcut window"
msgid "Accounts"
msgstr "Cuentas"

#: data/resources/ui/shortcuts.ui:43
msgctxt "shortcut window"
msgid "New Account"
msgstr "Nueva cuenta"

#: data/resources/ui/shortcuts.ui:49
msgctxt "shortcut window"
msgid "Search"
msgstr "Buscar"

#: data/resources/ui/shortcuts.ui:55
msgctxt "shortcut window"
msgid "Show Providers List"
msgstr "Mostrar la lista de proveedores"

#: data/resources/ui/window.ui:6
msgid "_Lock the Application"
msgstr "_Bloquear la aplicación"

#: data/resources/ui/window.ui:12
msgid "P_roviders"
msgstr "P_roveedores"

#: data/resources/ui/window.ui:16
msgid "_Preferences"
msgstr "_Preferencias"

#: data/resources/ui/window.ui:20
msgid "_Backup & Restore"
msgstr "_Respaldar y restaurar"

#: data/resources/ui/window.ui:26
msgid "_Keyboard Shortcuts"
msgstr "_Atajos de teclado"

#: data/resources/ui/window.ui:30
msgid "_About Authenticator"
msgstr "_Acerca de Autenticador"

#: data/resources/ui/window.ui:70
msgid "Authenticator is Locked"
msgstr "Autenticador está bloqueado"

#: data/resources/ui/window.ui:96
msgid "_Unlock"
msgstr "Desbloq_uear"

#: data/resources/ui/window.ui:126
msgid "Accounts"
msgstr "Cuentas"

#: data/resources/ui/window.ui:141 data/resources/ui/window.ui:179
msgid "New Account"
msgstr "Nueva cuenta"

#: data/resources/ui/window.ui:149 data/resources/ui/window.ui:192
msgid "Main Menu"
msgstr "Menú principal"

#: data/resources/ui/window.ui:157
msgid "No Accounts"
msgstr "No hay cuentas"

#: data/resources/ui/window.ui:158
msgid "Add an account or scan a QR code first."
msgstr "Añada cuenta o analice un código QR primero."

#: src/application.rs:83
msgid "Accounts restored successfully"
msgstr "Cuentas restauradas correctamente"

#: src/application.rs:123
msgid "translator-credits"
msgstr ""
"Daniel Mustieles <daniel.mustieles@gmail.com>, 2019-2021\n"
"Óscar Fernández Díaz <oscfdezdz@tuta.io>, 2020\n"
"Rodrigo Lledó, <rodhos92@gmail.com>, 2020\n"
"advocatux <advocatux@airpost.net>, 2018\n"
"Adolfo Jayme Barrientos <fitojb@ubuntu.com>, 2018"

#: src/application.rs:434 src/widgets/accounts/row.rs:45
msgid "One-Time password copied"
msgstr "Contraseña de un sólo uso copiada"

#: src/application.rs:435
msgid "Password was copied successfully"
msgstr "La contraseña se ha copiado correctamente"

#. Translators: This is for making a backup for the aegis Android app.
#. Translators: This is for restoring a backup from the aegis Android app.
#: src/backup/aegis.rs:399 src/backup/aegis.rs:439
msgid "Aegis"
msgstr "Aegis"

#: src/backup/aegis.rs:403
msgid "Into a JSON file containing plain-text or encrypted fields"
msgstr ""
"Dentro de un archivo JSON que contiene texto en claro o campos cifrados"

#: src/backup/aegis.rs:443
msgid "From a JSON file containing plain-text or encrypted fields"
msgstr ""
"A partir de un archivo JSON que contiene texto en claro o campos cifrados"

#. Translators: This is for making a backup for the andOTP Android app.
#: src/backup/andotp.rs:79
msgid "a_ndOTP"
msgstr "a_ndOTP"

#: src/backup/andotp.rs:83
msgid "Into a plain-text JSON file"
msgstr "En un archivo JSON de texto sencillo"

#. Translators: This is for restoring a backup from the andOTP Android app.
#: src/backup/andotp.rs:127
msgid "an_dOTP"
msgstr "an_dOTP"

#: src/backup/andotp.rs:131 src/backup/bitwarden.rs:124 src/backup/legacy.rs:45
msgid "From a plain-text JSON file"
msgstr "Desde un archivo JSON de texto sencillo"

#: src/backup/bitwarden.rs:47
msgid "Unknown account"
msgstr "Cuenta desconocida"

#: src/backup/bitwarden.rs:55
msgid "Unknown issuer"
msgstr "Emisor desconocido"

#. Translators: This is for restoring a backup from Bitwarden.
#: src/backup/bitwarden.rs:120
msgid "_Bitwarden"
msgstr "_Bitwarden"

#: src/backup/freeotp.rs:18
msgid "_Authenticator"
msgstr "_Autenticador"

#: src/backup/freeotp.rs:22
msgid "Into a plain-text file, compatible with FreeOTP+"
msgstr "En un archivo de texto sencillo, compatible con FreeOTP+"

#: src/backup/freeotp.rs:51
msgid "A_uthenticator"
msgstr "A_uthenticador"

#: src/backup/freeotp.rs:55
msgid "From a plain-text file, compatible with FreeOTP+"
msgstr "Desde un archivo de texto sencillo, compatible con FreeOTP+"

#: src/backup/freeotp_json.rs:87
msgid "FreeOTP+"
msgstr "FreeOTP+"

#: src/backup/freeotp_json.rs:91
msgid "From a plain-text JSON file, compatible with FreeOTP+"
msgstr "Desde un archivo de texto plano JSON, compatible con FreeOTP+"

#: src/backup/google.rs:19
msgid "Google Authenticator"
msgstr "Autenticador de Google"

#: src/backup/google.rs:23
msgid "From a QR code generated by Google Authenticator"
msgstr "A partir de un código QR generado por Google Authenticator"

#. Translators: this is for restoring a backup from the old Authenticator
#. release
#: src/backup/legacy.rs:41
msgid "Au_thenticator (Legacy)"
msgstr "Au_tenticador (Antiguo)"

#: src/backup/raivootp.rs:102
msgid "Raivo OTP"
msgstr "Raivo OTP"

#: src/backup/raivootp.rs:106
msgid "From a ZIP export generated by Raivo OTP"
msgstr "Desde un archivo ZIP generado por Raivo OTP"

#: src/models/algorithm.rs:60
msgid "Counter-based"
msgstr "Basado en contador"

#: src/models/algorithm.rs:61
msgid "Time-based"
msgstr "Basado en la hora"

#. Translators: Steam refers to the gaming application by Valve.
#: src/models/algorithm.rs:63
msgid "Steam"
msgstr "Steam"

#: src/models/algorithm.rs:125
msgid "SHA-1"
msgstr "SHA-1"

#: src/models/algorithm.rs:126
msgid "SHA-256"
msgstr "SHA-256"

#: src/models/algorithm.rs:127
msgid "SHA-512"
msgstr "SHA-512"

#: src/widgets/accounts/add.rs:270 src/widgets/backup/dialog.rs:443
#: src/widgets/providers/page.rs:291
msgid "Image"
msgstr "Imagen"

#: src/widgets/accounts/add.rs:278 src/widgets/backup/dialog.rs:451
msgid "Select QR Code"
msgstr "Seleccionar código QR"

#: src/widgets/accounts/add.rs:296
msgid "Invalid Token"
msgstr "Testigo no válido"

#: src/widgets/accounts/details.rs:123
msgid "Are you sure you want to delete the account?"
msgstr "¿Está seguro de que quiere eliminar la cuenta?"

#: src/widgets/accounts/details.rs:124
msgid "This action is irreversible"
msgstr "Esta acción es irreversible"

#: src/widgets/accounts/details.rs:126
msgid "No"
msgstr "No"

#: src/widgets/accounts/details.rs:126
msgid "Yes"
msgstr "Sí"

#: src/widgets/backup/dialog.rs:163 src/widgets/backup/dialog.rs:245
msgid "Key / Passphrase"
msgstr "Clave/ frase de paso nueva"

#: src/widgets/backup/dialog.rs:174 src/widgets/backup/dialog.rs:257
msgid "Select File"
msgstr "Seleccionar archivo"

#: src/widgets/backup/dialog.rs:205
msgid "Failed to create a backup"
msgstr "Falló al crear el respaldo"

#: src/widgets/backup/dialog.rs:327
msgid "Failed to restore from camera"
msgstr "Falló la restaurar desde la cámara"

#: src/widgets/backup/dialog.rs:352
msgid "Failed to restore from a screenshot"
msgstr "Falló al restaurar desde una captura de pantalla"

#: src/widgets/backup/dialog.rs:372
msgid "Failed to restore from an image"
msgstr "Falló al restaurar desde una imagen"

#: src/widgets/backup/dialog.rs:396
msgid "Failed to restore from a file"
msgstr "Falló al restaurar desde un archivo"

#: src/widgets/preferences/password_page.rs:195
#: src/widgets/preferences/password_page.rs:237
msgid "Wrong Passphrase"
msgstr "Frase de paso incorrecta"

#: src/widgets/providers/dialog.rs:261
msgid "Provider created successfully"
msgstr "Proveedor creado correctamente"

#: src/widgets/providers/dialog.rs:271
msgid "Provider updated successfully"
msgstr "Proveedor actualizado correctamente"

#: src/widgets/providers/dialog.rs:287
msgid "Provider removed successfully"
msgstr "Proveedor quitado correctamente"

#: src/widgets/providers/page.rs:185
msgid "Editing Provider: {}"
msgstr "Editando proveedor: {}"

#: src/widgets/providers/page.rs:324
msgid "The provider has accounts assigned to it, please remove them first"
msgstr "eL proveedor tiene cuentas asignadas, elimínelas primero"

#: src/widgets/window.rs:115
msgid "Wrong Password"
msgstr "Contraseña incorrecta"

#~ msgid "Bilal Elmoussaoui"
#~ msgstr "Bilal Elmoussaoui"

#~ msgid "Dark Mode"
#~ msgstr "Modo oscuro"

#~ msgid "Whether the application should use a dark mode."
#~ msgstr "Indica si la aplicación usa un modo oscuro."

#~ msgid "Go Back"
#~ msgstr "Retroceder"

#~ msgid "_Edit"
#~ msgstr "_Editar"

#~ msgid "Appearance"
#~ msgstr "Aspecto"

#~ msgid "_Dark Mode"
#~ msgstr "Mo_do oscuro"

#~ msgid "Whether the application should use a dark mode"
#~ msgstr "Indica si la aplicación debe usar un modo oscuro"

#~ msgid "The key that will be used to decrypt the vault"
#~ msgstr "La clave usada para descifrar la bóveda"

#~ msgid "The key used to encrypt the vault"
#~ msgstr "La clave usada para cifrar la bóveda"

#~ msgid "Unable to restore accounts"
#~ msgstr "No se pueden restaurar las cuentas"

#~ msgid "Something went wrong"
#~ msgstr "Algo ha fallado"

#~ msgid "Couldn't find a QR code"
#~ msgstr "No se pudo encontrar un código QR"

#~ msgid "D_etails"
#~ msgstr "D_etalles"

#~ msgid "_Rename"
#~ msgstr "_Renombrar"

#~ msgid "Used to fetch the provider icon"
#~ msgstr "Usado para obtener el icono del proveedor"

#~ msgid "Provider setup instructions"
#~ msgstr "Instrucciones de configuración del proveedor"

#~ msgid "Select"
#~ msgstr "Seleccionar"

#~ msgid "More"
#~ msgstr "Más"

#, fuzzy
#~| msgid "There are no accounts yet…"
#~ msgid "There Are no Accounts Currently"
#~ msgstr "No hay ninguna cuenta aún…"

#~ msgid "Huge database of more than 560 supported services"
#~ msgstr "Base de datos enorme con más de 560 dispositivos soportados"

#~ msgid ""
#~ "Keep your PIN tokens secure by locking the application with a password"
#~ msgstr ""
#~ "Mantenga sus testigos de PIN seguros bloqueando la aplicación con una "
#~ "contraseña"

#~ msgid "The possibility to add new services"
#~ msgstr "La posibilidad de añadir servicios nuevos"

#~ msgid "Dark theme and night light"
#~ msgstr "Tema oscuro y luz nocturna"

#~ msgid "Sharing an account"
#~ msgstr "Compartir una cuenta"

#~ msgid "Add"
#~ msgstr "Añadir"

#~ msgid "Share"
#~ msgstr "Compartir"

#~ msgid "By setting a password, Authenticator will from now on start locked."
#~ msgstr ""
#~ "Al establecer una contraseña, Authenticator comenzará a partir de ahora a "
#~ "estar bloqueado."

#~ msgid "The duration in seconds. The recommended value is 30"
#~ msgstr "La duración en segundos. El valor recomendado es 30"

#~ msgid "How to set up Two-Factor Authentication"
#~ msgstr "Cómo establecer la autenticación de doble factor"

#~ msgid "Add a new account from the menu"
#~ msgstr "Añadir una cuenta nueva desde el menú"

#~ msgid "Scan a QR Code"
#~ msgstr "Escanear un código QR"

#~ msgctxt "shortcut window"
#~ msgid "Show Shortcuts"
#~ msgstr "Mostrar atajos"

#~ msgctxt "shortcut window"
#~ msgid "Add"
#~ msgstr "Añadir"

#~ msgctxt "shortcut window"
#~ msgid "Scan QR"
#~ msgstr "Escanear código QR"

#~ msgid "_Providers"
#~ msgstr "_Proveedores"
